Numerical Derivatives in Scilab

Abstract
-------

This document present the use of numerical derivatives
in Scilab.
In the first part, we briefly report the first order forward formula, which 
is based on the Taylor theorem.
We then present the naive algorithm based on these mathematical formulas. 
In the second part, we make some experiments in Scilab and compare our
naive algorithm with the \emph{derivative} Scilab primitive.
In the third part, we analyse 
why and how floating point numbers must be taken into account when the 
numerical derivatives are to compute.

Author
-------

 Copyright (C) 2008-2011 - Michael Baudin

TODO
-------

 * Add a section on automatic differentiation.

Licence
-------

This document is released under the terms of the Creative Commons Attribution-ShareAlike 3.0 Unported License :

http://creativecommons.org/licenses/by-sa/3.0/

