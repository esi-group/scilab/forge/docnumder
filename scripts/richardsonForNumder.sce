// Copyright (C) 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// Uses Richardson's extrapolation method to 
// compute numerical derivative. 
// Uses a basic centered finite difference with 
// 2 points as the basic method. 



// Computes the first derivative with a centered F.D. with 2 points
function fp = centeredFD2pts ( f , x , h )
    fp = ( f(x+h) - f(x-h))/(2*h)
endfunction


// Computes the relative error of the F.D. formula at point x 
// with step h.
function y = relativeerror ( f , fp , x , h , fpapprox)
    expected = fp ( x )
    y = abs ( fpapprox - expected ) / abs( expected )
    y = min(y,1)
endfunction

// Computes the first derivative with Richardson's extrapolation.
//
// Parameters
// f : a function, with calling sequence y = f(x)
// x : a 1-by-1 matrix of doubles, the point where to compute the derivative
// h1 : a 1-by-1 matrix of doubles, the step
// n : a 1-by-1 matrix of doubles, integer value, number of levels in the extrapolation
// fp : a 1-by-1 matrix of doubles, the first derivative
//
// Description
// Computes the first derivative with Richardson's extrapolation. 
// This  is based on the section "2.3 Application to Numerical Differentiation" 
// of Sidi's "Practical extrapolation methods".
//
// Bibliography
// Practical extrapolation methods, Avram Sidi, Cambridge University Press, 2003
// 
//
function [fp,B] = richardsonNumder(f,x,h1,k)
    error("This function does not work !!!")
    // Compute the steps
    h(1) = h1
    for j = 2 : k+1
        h(j) = h(j-1)/2
    end
    //
    // Compute the centered finite differences
    for j = 1 : k
        B(1,j) = centeredFD2pts ( f , x , h(j) )
    end
    //
    // Use the Romberg table
    for n = 2 : k
        for j = 1 : k-n+1
            B(n,j) = h(j)^2*B(n-1,j+1) - h(j+n)^2*B(n-1,j)
            B(n,j) = B(n,j) / (h(j)^2 - h(j+n)^2)
        end        
    end
    //
    // Extract the best estimate
    fp = B(k,1)
endfunction


///////////////////////////////////////////////////////////
//
// Applies the theory on the sqrt function
//

// The square root function
function y = mysqrt ( x )
    y = sqrt(x)
endfunction

// The derivative of the square root function.
function y = mydsqrt ( x )
    y = 0.5 * x^(-0.5)
endfunction

x = 1;
h1 = 1/8;
k=10;
[fp,B] = richardsonNumder(mysqrt,x,h1,k)
