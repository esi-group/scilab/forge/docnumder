// Copyright (C) 2008-2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// Consider various formulas to compute the first derivative
// of a scalar function of one parameter.
// Compare the relative error, depending on the step.
// Compare:
// * forward (order 1)
// * centered 2 points (order 2)
// * centered 4 points (order 4)
//

// Computes the approximate step for the forward F.D. formula.
// Uses the unscaled step.
function y = step_approxUnscaled ( x )
    y = sqrt(%eps)
endfunction


// Computes the first derivative with a forward F.D.
function y = forwardFD ( f , x , h )
    y = ( f(x+h) - f(x))/h
endfunction

// Computes the first derivative with a centered F.D. with 2 points
function y = centeredFD2pts ( f , x , h )
    y = ( f(x+h) - f(x-h))/(2*h)
endfunction

// Computes the first derivative with a centered F.D. with 4 points
function y = centeredFD4pts ( f , x , h )
    y = ( 8*f(x+h) - 8*f(x-h) - f(x+2*h) + f(x-2*h))/(12*h)
endfunction


// Computes the relative error of the F.D. formula at point x 
// with step h.
function y = relativeerror ( f , fp , x , h , fpapprox)
    expected = fp ( x )
    y = abs ( fpapprox - expected ) / abs( expected )
    y = min(y,1)
endfunction

// Plots the relative error in log scale for given f at point x.
// Considers various F.D. formulas.
// The steps h are chosen in the range log(hlogrange)
function drawrelativeerror ( f , fp , x , mytitle , hlogrange )
    n = 1000;
    //
    // Plot computed relative error of forward F.D.
    logharray = linspace (hlogrange(1),hlogrange(2),n);
    for i = 1:n
        h = 10^(logharray(i));
        //
        fpapprox = forwardFD ( f , x , h )
        relerr = relativeerror ( f , fp , x , h , fpapprox );
        logearrayForward ( i ) = log10 ( relerr );
        //
        fpapprox = centeredFD2pts ( f , x , h )
        relerr = relativeerror ( f , fp , x , h , fpapprox );
        logearrayCentered2pts ( i ) = log10 ( relerr );
        //
        fpapprox = centeredFD4pts ( f , x , h )
        relerr = relativeerror ( f , fp , x , h , fpapprox );
        logearrayCentered4pts ( i ) = log10 ( relerr );
    end
    plot ( logharray , logearrayForward , "b-")
    plot ( logharray , logearrayCentered2pts , "r--")
    plot ( logharray , logearrayCentered4pts , "g-.")
    //
    // Put the title
    xtitle(mytitle,"log10(h)","log10(RE)");
    legend(["Forward","Centered 2 pts","Centered 4 pts"])
endfunction


///////////////////////////////////////////////////////////
//
// Applies the theory on the sqrt function
//

// The square root function
function y = mysqrt ( x )
    y = sqrt(x)
endfunction

// The derivative of the square root function.
function y = mydsqrt ( x )
    y = 0.5 * x^(-0.5)
endfunction


// Try in x = 1
// Plot the Relative Error
x = 1.0;
scf();
mytitle = "Numerical derivative of sqrt in x=1.0";
drawrelativeerror ( mysqrt , mydsqrt , x , mytitle , [-16,0] );

