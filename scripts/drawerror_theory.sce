// Copyright (C) 2008-2009 - INRIA - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// Consider the forward formula to compute the first derivative
// of a scalar function of one parameter.

// Draw the total error function, depending on the step.
// Consider the function f(x) = x^2, near x = 1.
f = 1;
fpp = 2;
function e = totalerror ( h ) 
  r = %eps
  e = r * abs(f) / h + h * abs(fpp) / 2.0
endfunction

n = 1000;
x = linspace (-16,0,n);
y = zeros(n,1);
for i = 1:n
  h = 10^(x(i));
  y(i) = log10(totalerror ( h ));
end
plot ( x , y )

f = gcf();
f.children.title.text="Total error of numerical derivative";
f.children.x_label.text = "log(h)";
f.children.y_label.text = "log(E)";

