// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// Evaluates f.
function y = derivativeEvalf(__derEvalf__,x)
    if ( typeof(__derEvalf__)=="function" ) then
        y = __derEvalf__(x)
    elseif ( typeof(__derEvalf__)=="list" ) then
        __f_fun__ = __derEvalf__(1)
        y = __f_fun__(x,__derEvalf__(2:$))
    else
        error(msprintf("Unknown function type %s",typeof(f)))
    end
endfunction

// A collection of formulas for the Jacobian
function J = derivativeJacobian(__derJacf__,x,h,form)
    n = size(x,"*")
    D = diag(h)
    for i = 1 : n
        d = D(:,i)
        select form
        case "forward2points" then   // Order 1
            y(:,1) = -derivativeEvalf(__derJacf__,x)
            y(:,2) = derivativeEvalf(__derJacf__,x+d)
        case "backward2points" then    // Order 1
            y(:,1) = derivativeEvalf(__derJacf__,x)
            y(:,2) = -derivativeEvalf(__derJacf__,x-d)
        case "centered2points" then    // Order 2
            y(:,1) = 1/2*derivativeEvalf(__derJacf__,x+d)
            y(:,2) = -1/2*derivativeEvalf(__derJacf__,x-d)
        case "doubleforward3points" then    // Order 2
            y(:,1) = -3/2*derivativeEvalf(__derJacf__,x)
            y(:,2) = 2*derivativeEvalf(__derJacf__,x+d)
            y(:,3) = -1/2*derivativeEvalf(__derJacf__,x+2*d)
        case "doublebackward3points" then    // Order 2
            y(:,1) = 3/2*derivativeEvalf(__derJacf__,x)
            y(:,2) = -2*derivativeEvalf(__derJacf__,x-d)
            y(:,3) = 1/2*derivativeEvalf(__derJacf__,x-2*d)
        case "centered4points" then    // Order 4
            y(:,1) = -1/12*derivativeEvalf(__derJacf__,x+2*d)
            y(:,2) = 2/3*derivativeEvalf(__derJacf__,x+d)
            y(:,3) = -2/3*derivativeEvalf(__derJacf__,x-d)
            y(:,4) = 1/12*derivativeEvalf(__derJacf__,x-2*d)
        else
            error(msprintf("Unknown formula %s",form))
        end
        J(:,i) = sum(y,"c")/h(i)
    end
endfunction

// The approximate unscaled steps for the Jacobian
function h = derivativeJacobianStep(form)
    select form
    case "forward2points" then    // Order 1
        h=%eps^(1/2)
    case "backward2points" then    // Order 1
        h=%eps^(1/2)
    case "centered2points" then    // Order 2
        h=%eps^(1/3)
    case "doubleforward3points" then    // Order 2
        h=%eps^(1/3)
    case "doublebackward3points" then    // Order 2
        h=%eps^(1/3)
    case "centered4points" then    // Order 4
        h=%eps^(1/5)
    else
        error(msprintf("Unknown formula %s",form))
    end
endfunction

// The approximate unscaled steps for the Hessian
function h = derivativeHessianStep(form)
    select form
    case "forward2points" then    // Order 1
        h=%eps^(1/3)
    case "backward2points" then    // Order 1
        h=%eps^(1/3)
    case "centered2points" then    // Order 2
        h=%eps^(1/4)
    case "doubleforward3points" then    // Order 2
        h=%eps^(1/4)
    case "doublebackward3points" then    // Order 2
        h=%eps^(1/4)
    case "centered4points" then    // Order 4
        h=%eps^(1/6)
    else
        error(msprintf("Unknown formula %s",form))
    end
endfunction

//
// Test with a vectorial function
//

function y = quadf ( x )
    f1 = x(1)^2 + x(2)^3 + x(3)^4
    f2 = exp(x(1)) + 2*sin(x(2)) + 3*cos(x(3))
    y = [f1;f2]
endfunction

function J = quadJ ( x )
    J1(1) = 2 * x(1)
    J1(2) = 3 * x(2)^2
    J1(3) = 4 * x(3)^3
    //
    J2(1) = exp(x(1))
    J2(2) = 2*cos(x(2))
    J2(3) = -3*sin(x(3))
    //
    J = [J1';J2']
endfunction

function H = quadH ( x )
    H1 = [
    2	0		0
    0	6*x(2)	0
    0	0		12*x(3)^2
    ]
    //
    H2 = [
    exp(x(1)) 0 0
    0 -2*sin(x(2)) 0
    0 0 -3*cos(x(3))
    ]
    //
    H = [H1;H2]
endfunction


x=[1;2;3];
J = quadJ ( x );
mprintf("Jexact = \n")
disp(J);

////////////////////////////////////////////////////////
//
// Test higher accuracy with arbitrary step 
// and "forward2points".
//

x=[1;2;3];
h = 1.e-2*ones(3,1);

form = "forward2points";

// With h: Order 1
Japprox1 = derivativeJacobian(quadf,x,h,form);
d = assert_computedigits(J,Japprox1)
// With 2h: Order 1
Japprox2 = derivativeJacobian(quadf,x,2*h,form);
// Combine: Order 2
a = 1;
Japprox = (2^a*Japprox1-Japprox2)/(2^a-1);
d = assert_computedigits(J,Japprox)

// Improvement in accuracy.

////////////////////////////////////////////////////////
//
// Test higher accuracy with approximate optimal step 
// and "forward2points".
//

x=[1;2;3];

// The initial formula is order 1.
// The modified formula is order at least 2.
// Therefore, the approximate optimal step is:
h = %eps^(1/3);
h = h*ones(3,1);

form = "forward2points";

// With h: Order 1
Japprox1 = derivativeJacobian(quadf,x,h,form);
d = assert_computedigits(J,Japprox1)
// With 2h: Order 1
Japprox2 = derivativeJacobian(quadf,x,2*h,form);
// Combine: Order 2
a = 1;
Japprox = (2^a*Japprox1-Japprox2)/(2^a-1);
d = assert_computedigits(J,Japprox)

// Huge improvement in accuracy.

////////////////////////////////////////////////////////
//
// Test higher accuracy with approximate optimal step 
// and "centered4points".
//

x=[1;2;3];

// The initial formula is order 4.
// The modified formula is order at least 5.
// Therefore, the approximate optimal step is:
h = %eps^(1/6);
h = h*ones(3,1);

form = "centered4points";

// With h: Order 4
Japprox1 = derivativeJacobian(quadf,x,h,form);
d = assert_computedigits(J,Japprox1)
// With 2h: Order 4
Japprox2 = derivativeJacobian(quadf,x,2*h,form);
// Combine: Order 5
a = 4;
Japprox = (2^a*Japprox1-Japprox2)/(2^a-1);
d = assert_computedigits(J,Japprox)

// No improvement in accuracy.
