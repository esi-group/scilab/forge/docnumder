// Copyright (C) 2008-2009 - Michael Baudin

// Compute the derivative with a naive method and compare with 
// the derivative function.
function fp = myfprime(f,x,h)
  fp = (f(x+h) - f(x))/h;
endfunction
function y = myfunction (x)
  y = x*x;
endfunction
fpref = derivative(myfunction,x);
e = abs(fpref-2.0)/2.0;
mprintf("Scilab f''=%e, error=%e\n", fpref,e);
h = 1.e-16;
fp = myfprime(myfunction,x,h);
e = abs(fp-2.0)/2.0;
mprintf("Naive f''=%e, h=%e, error=%e\n", fp,h,e);

// Perform a loop with decreasing step sizes
x = 1.0;
fpref = derivative(myfunction,x,order=1);
e = abs(fpref-2.0)/2.0;
mprintf("Scilab f''=%e, error=%e\n", fpref,e);
h = 1.0;
for i=1:20
  h=h/10.0;
  fp = myfprime(myfunction,x,h);
  e = abs(fp-2.0)/2.0;
  mprintf("Naive f''=%e, h=%e, error=%e\n", fp,h,e);
end

// Show derivative of a multivariate function
function y = myfunction2 (x)
  y = x.' * x;
endfunction
x = ones(10,1);
J = derivative(myfunction2,x);

// Show derivative of a univariate function with varying formula orders
function y = myfunction3 (x)
  y = x^2;
endfunction
x = 1.0;
expected = 2.0;
for o = [1 2 4]
  fp = derivative(myfunction3,x, order = o);
  err = abs(fp-expected)/abs(expected);
  mprintf("Order = %d, Relative error : %e\n",o,err)
end

// Check the number of function evaluations which are necessary for gradient
function y = myfunction3 (x)
  global nbfeval
  nbfeval = nbfeval + 1
  y = x.' * x;
endfunction
x = (1:10).';
for o = [1 2 4]
  global nbfeval
  nbfeval = 0;
  J = derivative(myfunction3,x,order=o);
  mprintf("Order = %d, Number of function evaluations : %d\n",order,nbfeval)
end

// Check the number of function evaluations which are necessary for gradient and Hessian
function y = myfunction3 (x)
  global nbfeval
  nbfeval = nbfeval + 1
  y = x.' * x;
endfunction
x = (1:10).';
for o = [1 2 4]
  global nbfeval
  nbfeval = 0;
  [ J , H ] = derivative(myfunction3,x,order=o);
  mprintf("Order = %d, Number of function evaluations : %d\n",o,nbfeval)
end

// Use various directions and show the effect of the matrix Q
a = 1.e20;
function y = myfunction4 (x)
  y = x(1)^2 + a * x(2)^2;
endfunction
x = [1 1].';
g = derivative(myfunction4,x);
mprintf("Q #0, g(1) = %e, g(2) = %e\n",g(1),g(2))
for i = 1:3
  Q = qr(rand(2,2));
  g = derivative(myfunction4,x,Q = Q);
  mprintf("Q #%d, g(1) = %e, g(2) = %e\n",i,g(1),g(2))
end

// Difference between derivative and numdiff when x is large
function y = myfunction (x)
  y = x*x;
endfunction
x = 1.e100;
fe = 2.0 * x;
fp = derivative(myfunction,x);
e = abs(fp-fe)/fe;
mprintf("Relative error with derivative: %e\n",e)
fp = numdiff(myfunction,x);
e = abs(fp-fe)/fe;
mprintf("Relative error with numdiff: %e\n",e)

// Difference between derivative, nummdiff and exact when x is small
function y = myfunction (x)
  y = x*x;
endfunction
x = 1.e-100;
fe = 2.0 * x;
fp = derivative(myfunction,x);
e = abs(fp-fe)/fe;
mprintf("Relative error with derivative: %e\n",e)
fp = numdiff(myfunction,x);
e = abs(fp-fe)/fe;
mprintf("Relative error with numdiff: %e\n",e)

// Use splin to get derivatives of a univariate function
function y = myfunction (x)
  // This is vectorized
  y = exp(x);
endfunction
h = sqrt(%eps);
x = 1.0;
fe = exp(1);
// Test with 5 points
xx = [x-2*h x-h x x+h x+2*h];
yy = myfunction ( xx );
d5 = splin ( xx , yy );
fp = d5 ( 3 );
e = abs(fp-fe)/fe;
mprintf("Relative error on f'' with splin (5 points): %e\n",e)
// Test with 3 points
xvector = [x-h x x+h];
yvector = myfunction ( xvector );
d3 = splin ( xvector , yvector );
fp = d3(2);
e = abs(fp-fe)/fe;
mprintf("Relative error on f'' with splin (3 points): %e\n",e)

// Get higher derivatives of a univariate function with splin
function y = myfunction (x)
  // This is vectorized
  y = exp(x);
endfunction
h = sqrt(%eps);
x = 1.0;
xvector = [x-2*h x-h x x+h x+2*h];
yvector = myfunction ( xvector );
dk = splin ( xvector , yvector );
[f, fp, fpp] = interp ( x , xvector , yvector , dk ); 
fe = exp(1);
e = abs(fp-fe)/fe;
mprintf("Relative error on f'' with splin (5 points): %e\n",e)
fe = exp(1);
e = abs(fpp-fe)/fe;
mprintf("Relative error on f'''' with splin (5 points): %e\n",e)



